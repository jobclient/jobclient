import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { JobSummary, JobPosting, JobApplication, JobFollowup, JobMemo, JobRecord } from '@jobclient/model';

import { SnackBarHelper } from '../../ui/snack-bar-helper';
import { JobclientViewDataService } from '../../services/jobclient-view-data.service';

import { JobDetailRecordComponent } from '../../elements/job-detail-record/job-detail-record.component';


@Component({
  selector: 'jc-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DetailPageComponent implements OnInit, AfterViewInit {

  @ViewChild("jcJobDetailRecord") jcJobDetailRecord: JobDetailRecordComponent;
  
  constructor(
    private snackBarHelper: SnackBarHelper,
    private viewDataService: JobclientViewDataService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.snackBarHelper.showQuickMessage('Not implemented yet', 'WIP');

  }


}
