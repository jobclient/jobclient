import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { IUserLoginService, IUserRegistrationService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';

import { SnackBarHelper } from '../../ui/snack-bar-helper';


import {
  NewRegistrationModalComponent,
  ResendCodeModalComponent,
  ConfirmRegistrationModalComponent
} from '@ngauth/modals';

import { AuthStateHelper } from '../../helpers/auth-state-helper';


@Component({
  selector: 'jc-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit, AfterViewInit {

  private registrationService: IUserRegistrationService;
  private loginService: IUserLoginService;

  constructor(
    private dialog: MatDialog,
    private userAuthServiceFactory: UserAuthServiceFactory,
    private snackBarHelper: SnackBarHelper,
  ) { 
    this.registrationService = this.userAuthServiceFactory.getUserRegistrationService();
    this.loginService = this.userAuthServiceFactory.getUserLoginService();
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.snackBarHelper.showQuickMessage('Not implemented yet', 'WIP');

  }


  get isUserLoggedIn(): boolean {
    return AuthStateHelper.getInstance().userAuthenticated;
  }


  handleNewRegistration() {
    if(isDL()) dl.log("'Register' button clicked.");

    // TBD:
    this.openNewRegistrationDialog();
    // ...
  }

  openNewRegistrationDialog() {
    if(isDL()) dl.log('openNewRegistrationDialog button clicked.');

    let dialogRef = this.dialog.open(NewRegistrationModalComponent, {
      width: '320px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(isDL()) dl.log('The dialog was closed. result = ' + result);
    });
  }

  openResendCodeDialog() {
    if(isDL()) dl.log('openResendCodeDialog button clicked.');

    let dialogRef = this.dialog.open(ResendCodeModalComponent, {
      width: '320px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(isDL()) dl.log('The dialog was closed. result = ' + result);
    });
  }

  openConfirmRegistrationDialog() {
    if(isDL()) dl.log('openConfirmRegistrationDialog button clicked.');

    let dialogRef = this.dialog.open(ConfirmRegistrationModalComponent, {
      width: '280px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(isDL()) dl.log('The dialog was closed. result = ' + result);
    });
  }



}
