import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { JobSummary, JobPosting, JobApplication, JobFollowup, JobMemo, JobRecord } from '@jobclient/model';

import { SnackBarHelper } from '../../ui/snack-bar-helper';
import { JobclientViewDataService } from '../../services/jobclient-view-data.service';

import { RecordSearchQueryComponent } from '../../elements/record-search-query/record-search-query.component';
import { RecordSearchResultComponent } from '../../elements/record-search-result/record-search-result.component';


@Component({
  selector: 'jc-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SearchPageComponent implements OnInit, AfterViewInit {

  @ViewChild("jcRecordSearchQuery") jcRecordSearchQuery: RecordSearchQueryComponent;
  @ViewChild("jcRecordSearchResult") jcRecordSearchResult: RecordSearchResultComponent;
  
  constructor(
    private snackBarHelper: SnackBarHelper,
    private viewDataService: JobclientViewDataService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.snackBarHelper.showQuickMessage('Not implemented yet', 'WIP');

  }


}
