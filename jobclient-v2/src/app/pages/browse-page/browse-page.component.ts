import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { JobSummary, JobPosting, JobApplication, JobFollowup, JobMemo, JobRecord } from '@jobclient/model';

import { SnackBarHelper } from '../../ui/snack-bar-helper';
import { JobclientViewDataService } from '../../services/jobclient-view-data.service';

import { MiniJobRecordComponent } from '../../elements/mini-job-record/mini-job-record.component';
import { JobPostingEditorComponent } from '../../elements/job-posting-editor/job-posting-editor.component';


@Component({
  selector: 'jc-browse-page',
  templateUrl: './browse-page.component.html',
  styleUrls: ['./browse-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BrowsePageComponent implements OnInit, AfterViewInit {

  // @ViewChild("jcMiniJobRecord") jcMiniJobRecord: MiniJobRecordComponent;
  // @ViewChild("jcJobPostingEditor") jcJobPostingEditor: JobPostingEditorComponent;
  
  constructor(
    private snackBarHelper: SnackBarHelper,
    private viewDataService: JobclientViewDataService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.snackBarHelper.showQuickMessage('Not implemented yet', 'WIP');

  }


}
