import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { JobSummary, JobPosting, JobApplication, JobFollowup, JobMemo, JobRecord } from '@jobclient/model';

import { SnackBarHelper } from '../../ui/snack-bar-helper';
import { JobclientViewDataService } from '../../services/jobclient-view-data.service';

import { PreferredJobListComponent } from '../../elements/preferred-job-list/preferred-job-list.component';
import { RecentJobListComponent } from '../../elements/recent-job-list/recent-job-list.component';


@Component({
  selector: 'jc-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardPageComponent implements OnInit, AfterViewInit {

  @ViewChild("jcPreferredJobList") jcPreferredJobList: PreferredJobListComponent;
  @ViewChild("jcRecentJobList") jcRecentJobList: RecentJobListComponent;
  
  constructor(
    private snackBarHelper: SnackBarHelper,
    private viewDataService: JobclientViewDataService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.snackBarHelper.showQuickMessage('Not implemented yet', 'WIP');

  }


}
