import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { JobSummary, JobPosting, JobApplication, JobFollowup, JobMemo, JobRecord } from '@jobclient/model';

import { SnackBarHelper } from '../../ui/snack-bar-helper';
import { JobclientViewDataService } from '../../services/jobclient-view-data.service';

import { FavoriteJobRecordComponent } from '../../elements/favorite-job-record/favorite-job-record.component';


@Component({
  selector: 'jc-favorites-page',
  templateUrl: './favorites-page.component.html',
  styleUrls: ['./favorites-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FavoritesPageComponent implements OnInit, AfterViewInit {

  @ViewChild("jcFavoriteJobRecord") jcFavoriteJobRecord: FavoriteJobRecordComponent;
  
  constructor(
    private snackBarHelper: SnackBarHelper,
    private viewDataService: JobclientViewDataService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.snackBarHelper.showQuickMessage('Not implemented yet', 'WIP');

  }


}
