import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { JobSummary, JobPosting, JobApplication, JobFollowup, JobMemo, JobRecord } from '@jobclient/model';

import { SnackBarHelper } from '../../ui/snack-bar-helper';
import { JobclientViewDataService } from '../../services/jobclient-view-data.service';

import { ActiveJobNamesComponent } from '../../elements/active-job-names/active-job-names.component';
import { JobDetailRecordComponent } from '../../elements/job-detail-record/job-detail-record.component';


@Component({
  selector: 'jc-workspace-page',
  templateUrl: './workspace-page.component.html',
  styleUrls: ['./workspace-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WorkspacePageComponent implements OnInit, AfterViewInit {

  @ViewChild("jcActiveJobNames") jcActiveJobNames: ActiveJobNamesComponent;
  @ViewChild("jcJobDetailRecord") jcJobDetailRecord: JobDetailRecordComponent;

  constructor(
    private snackBarHelper: SnackBarHelper,
    private viewDataService: JobclientViewDataService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.snackBarHelper.showQuickMessage('Not implemented yet', 'WIP');

  }


}
