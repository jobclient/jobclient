import { Injectable } from '@angular/core';

// @Injectable()
export class AuthStateHelper {
  // Singleton.
  private static _Instance: (AuthStateHelper | null) = null;
  private constructor() { }
  public static getInstance(): AuthStateHelper {
    return this._Instance || (this._Instance = new AuthStateHelper());
  }


  private _authConfLoaded: boolean = false;
  public get authConfLoaded(): boolean {
    return this._authConfLoaded;
  }
  public set authConfLoaded(_authConfLoaded:boolean) {
    this._authConfLoaded = _authConfLoaded;
  }

  private _userAuthenticated: boolean = false;
  public get userAuthenticated(): boolean {
    return this._userAuthenticated;
  }
  public set userAuthenticated(_userAuthenticated:boolean) {
    this._userAuthenticated = _userAuthenticated;
  }

}
