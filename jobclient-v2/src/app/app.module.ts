import { NgModule, Inject } from '@angular/core';
import { APP_INITIALIZER } from '@angular/core';
import { BrowserModule, DOCUMENT } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
// import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
// import { InMemoryDataService } from './in-memory-data.service';
import { ServiceWorkerModule } from '@angular/service-worker';

import { MaterialComponentsModule } from './material-components.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { Reducers } from './redux/reducers';
import { RouterEffects } from './redux/effects/router-effects';

import { AppConfig } from './config/app.config';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';
import { NgCoreAuthModule } from '@ngcore/auth';
import { NgCoreHuesModule } from '@ngcore/hues';
import { NgCoreIdleModule } from '@ngcore/idle';
import { NgCoreMarkModule } from '@ngcore/mark';
import { NgCoreTimeModule } from '@ngcore/time';
import { NgCoreViewModule } from '@ngcore/view';
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';
import { NgAuthFormsModule } from '@ngauth/forms';
import { NgAuthModalsModule } from '@ngauth/modals';
import { NgAuthCognitoModule } from '@ngauth/cognito';
import { JobclientModelModule } from '@jobclient/model';
import { JobclientStoreModule } from '@jobclient/store';
import { JobclientSearchModule } from '@jobclient/search';

import { GlobalThemeHelper } from '@ngcore/hues';

import { DefaultCognitoAuthConf } from '@ngauth/core';
import { AuthConfigManager } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import { CognitoUserAuthServiceFactory } from '@ngauth/cognito';
// import { CognitoCommonUtil, DdbServiceUtil, AwsServiceHelper } from '@ngauth/cognito';
import { UserRegistrationHelper, UserLoginHelper, UserAttributesHelper } from '@ngauth/cognito';

import { environment } from '../environments/environment';

import { AppCognitoAuthConf } from './config/app-cognito-auth-conf';
// import { GlobalThemeHelper } from './helpers/global-theme-helper';
import { SnackBarHelper } from './ui/snack-bar-helper';
import { JobclientViewDataService } from './services/jobclient-view-data.service';


import { PreferredJobListComponent } from './elements/preferred-job-list/preferred-job-list.component';
import { RecentJobListComponent } from './elements/recent-job-list/recent-job-list.component';
import { ActiveJobNamesComponent } from './elements/active-job-names/active-job-names.component';
import { JobDetailPostingComponent } from './elements/job-detail-posting/job-detail-posting.component';
import { JobDetailApplicationComponent } from './elements/job-detail-application/job-detail-application.component';
import { JobDetailFollowupComponent } from './elements/job-detail-followup/job-detail-followup.component';
import { JobDetailMemoComponent } from './elements/job-detail-memo/job-detail-memo.component';
import { JobDetailSummaryComponent } from './elements/job-detail-summary/job-detail-summary.component';
import { JobDetailRecordComponent } from './elements/job-detail-record/job-detail-record.component';
import { RecordSearchQueryComponent } from './elements/record-search-query/record-search-query.component';
import { RecordSearchResultComponent } from './elements/record-search-result/record-search-result.component';
import { MiniJobRecordComponent } from './elements/mini-job-record/mini-job-record.component';
import { JobPostingEditorComponent } from './elements/job-posting-editor/job-posting-editor.component';
import { FavoriteJobRecordComponent } from './elements/favorite-job-record/favorite-job-record.component';
import { AuthUserLoginComponent } from './elements/auth-user-login/auth-user-login.component';
import { AuthUserLogoutComponent } from './elements/auth-user-logout/auth-user-logout.component';
import { UserPrefsGeneralComponent } from './elements/user-prefs-general/user-prefs-general.component';

import { NotFoundComponent } from './errors/not-found/not-found.component';

import { AppComponent } from './main/app.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { WorkspacePageComponent } from './pages/workspace-page/workspace-page.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { BrowsePageComponent } from './pages/browse-page/browse-page.component';
import { DetailPageComponent } from './pages/detail-page/detail-page.component';
import { FavoritesPageComponent } from './pages/favorites-page/favorites-page.component';

import { AppRoutingModule } from './app-routing.module';
import { PostingEditorDialogComponent } from './modals/posting-editor-dialog/posting-editor-dialog.component';
import { UserProfileDialogComponent } from './modals/user-profile-dialog/user-profile-dialog.component';
import { UserSettingsDialogComponent } from './modals/user-settings-dialog/user-settings-dialog.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardPageComponent,
    WorkspacePageComponent,
    SearchPageComponent,
    BrowsePageComponent,
    DetailPageComponent,
    PreferredJobListComponent,
    RecentJobListComponent,
    ActiveJobNamesComponent,
    JobDetailPostingComponent,
    JobDetailApplicationComponent,
    JobDetailFollowupComponent,
    JobDetailMemoComponent,
    JobDetailSummaryComponent,
    JobDetailRecordComponent,
    RecordSearchQueryComponent,
    RecordSearchResultComponent,
    MiniJobRecordComponent,
    NotFoundComponent,
    JobPostingEditorComponent,
    FavoritesPageComponent,
    FavoriteJobRecordComponent,
    AuthUserLoginComponent,
    AuthUserLogoutComponent,
    UserPrefsGeneralComponent,
    PostingEditorDialogComponent,
    UserProfileDialogComponent,
    UserSettingsDialogComponent,
    RegisterPageComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    FormsModule, ReactiveFormsModule,
    // FlexLayoutModule,
    HttpClientModule,
    // HttpClientInMemoryWebApiModule.forRoot(
    //   InMemoryDataService, { dataEncapsulation: false }
    // ),
    MaterialComponentsModule,
    StoreModule.forRoot(
      Reducers
    ),
    EffectsModule.forRoot([
      RouterEffects
    ]),
    StoreRouterConnectingModule,
    AppRoutingModule,
    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot(),
    NgCoreAuthModule.forRoot(),
    NgCoreHuesModule.forRoot(),
    NgCoreIdleModule.forRoot(),
    NgCoreMarkModule.forRoot(),
    NgCoreTimeModule.forRoot(),
    NgCoreViewModule.forRoot(),
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot(),
    NgAuthFormsModule.forRoot(),
    NgAuthModalsModule.forRoot(),
    NgAuthCognitoModule.forRoot(),
    JobclientModelModule.forRoot(),
    JobclientStoreModule.forRoot(),
    JobclientSearchModule.forRoot()
  ],
  providers: [
    AppConfig,
    AppCognitoAuthConf,
    // { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load(), deps: [AppConfig], multi: true },
    { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load().then(o => { console.log("App config loaded."); }), deps: [AppConfig], multi: true },
    { provide: DefaultCognitoAuthConf, useClass: AppCognitoAuthConf },
    { provide: UserAuthServiceFactory, useClass: CognitoUserAuthServiceFactory },
    // GlobalThemeHelper,
    SnackBarHelper,
    JobclientViewDataService,
  ],
  entryComponents: [
    PostingEditorDialogComponent,
    UserProfileDialogComponent,
    UserSettingsDialogComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
  // TBD: How to speed up the first theme loading ???
  // (Currently, we are geting the following warning:
  //   "Could not find Angular Material core theme. Most Material components may not work as expected.")
  // Heck. -> Does this help at all???
  // private static theme = GlobalThemeHelper.getInstance().getTheme();

  constructor( 
    @Inject(DOCUMENT) private document,
    private authConfigManager: AuthConfigManager,
    private globalThemeHelper: GlobalThemeHelper
  ) {
    // // let theme = GlobalThemeHelper.getInstance().getTheme();
    // let theme = AppModule.theme;
    let theme = this.globalThemeHelper.getTheme();
    if (theme) {
      // this.document.getElementById('material-theme').setAttribute('href', theme);
      this.document.getElementById('global-theme').setAttribute('href', theme);
    }

    // Normally, it's best to call it here (or, in APP_INITIALIZER).
    // ...
    this.authConfigManager.triggerLoading();
    // ...
  }
}
