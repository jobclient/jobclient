import { Component, OnInit, Inject } from '@angular/core';
import { isDevMode } from '@angular/core';
// import { DOCUMENT } from '@angular/platform-browser';
import { Location } from '@angular/common'
import { Router, ActivatedRoute, NavigationEnd, ActivationEnd } from '@angular/router';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/filter';
// import { filter } from 'rxjs/operators';

import { MatDialog } from '@angular/material';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil } from '@ngcore/core';
import { BrowserWindowService } from '@ngcore/core';
import { GlobalThemeHandler } from '@ngcore/hues';

// Testing
import { LoggedInStatus } from '@ngauth/core';
import { AuthConfigCallback, ICognitoAuthConfig, DefaultCognitoAuthConf } from '@ngauth/core';
import { AuthConfigManager } from '@ngauth/services';
import { IUserLoginService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import {
  AuthReadyComponent,
  UserStateComponent,
  UserLoginComponent,
  UserLogoutComponent,
  NewRegistrationComponent,
  ConfirmRegistrationComponent,
  ResendCodeComponent,
  ChangePasswordComponent,
  ResetPasswordStep1Component, ResetPasswordStep2Component,
} from '@ngauth/forms';

import {
  UserLoginModalComponent,
  UserLogoutModalComponent,
  NewRegistrationModalComponent,
  ResendCodeModalComponent,
  ConfirmRegistrationModalComponent,
  ChangePasswordModalComponent,
  ResetPasswordModalComponent
} from '@ngauth/modals';


import { AppConfig } from '../config/app.config';
import { AppState } from '../redux/core/app-state';
import * as RouterActions from '../redux/actions/router';

// import { GlobalThemeHelper } from '../helpers/global-theme-helper';
import { AuthStateHelper } from '../helpers/auth-state-helper';

import { JobclientViewDataService } from '../services/jobclient-view-data.service';

import { UserProfileDialogComponent } from '../modals/user-profile-dialog/user-profile-dialog.component';
import { UserSettingsDialogComponent } from '../modals/user-settings-dialog/user-settings-dialog.component';
import { PostingEditorDialogComponent } from '../modals/posting-editor-dialog/posting-editor-dialog.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AuthConfigCallback {
  // // Heck. We are dynamically changing the theme at app startup time.
  // // Or, read it from the user prefs.
  // private static THEMES: string[] = [
  //   "deeppurple-amber.css",
  //   "indigo-pink.css",
  //   "pink-bluegrey.css",
  //   "purple-green.css"
  // ];
  // // Cache.
  // private static _theme: (string | null) = null;
  // private static getTheme(): string {
  //   if(AppComponent._theme == null) {
  //     // Pick a random theme of the day.
  //     // TBD: Read this from the user settings.
  //     let idx = Math.floor(Math.random() * AppComponent.THEMES.length);
  //     AppComponent._theme = AppComponent.THEMES[idx];
  //   }
  //   return AppComponent._theme;
  // }

  private loginService: IUserLoginService;
  // private authConfLoaded: boolean = false;
  // private userAuthenticated: boolean = false;

  // temporary
  headerMenuTooltipPosition = 'below';
  fabMenuTooltipPosition = 'below';
  footerMenuTooltipPosition = 'above';

  title: string;
  routerPath: string;
  // copyright: string;
  copyrightText: string;
  copyrightLink: string;

  get pageTitle(): string {
    let pgTtl = this.title;

    // // Exclude "/"
    // // tbd: if "/", then use Dashboard ???
    // if (this.routerPath != null && this.routerPath.length > 1) {
    //   pgTtl += ` - ${this.routerPath}`;
    // }

    let pgNm = this.routerPath;
    // if (this.routerPath == null || this.routerPath.length <= 1) {
    if (this.routerPath == '' || this.routerPath == '/') {
      pgNm = 'Dashboard';   // hard-coded. TBD: Need to fix this...
    }
    pgTtl += ` - ${pgNm}`;

    return pgTtl;
  }

  constructor(
    // @Inject(DOCUMENT) private document,
    private location: Location,
    private router: Router,
    // private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private dialog: MatDialog,
    private appConfig: AppConfig,
    private browserWindowService: BrowserWindowService,
    private authConfigManager: AuthConfigManager,
    private userAuthServiceFactory: UserAuthServiceFactory,
    private globalThemeHandler: GlobalThemeHandler,
    private viewDataService: JobclientViewDataService
  ) {
    // testing
    if (isDevMode()) {
      if(isDL()) dl.log("In dev mode.");
    } else {
      if(isDL()) dl.log("In prod mode.");
    }

    this.authConfigManager.addCallback(this);
    this.loginService = this.userAuthServiceFactory.getUserLoginService();
  }

  authConfigLoaded(): void {
    AuthStateHelper.getInstance().authConfLoaded = true;
    // if (this.ngAuthAuthReady) {
    //   this.ngAuthAuthReady.refreshUI();
    // }
    // if (this.ngAuthUserState) {
    //   // this.ngAuthUserState.refreshUI();
    //   this.ngAuthUserState.checkLoginStatus();
    // }
  }

  // TBD:
  // Is there a better way???
  private pageUrlPath = '';
  get isOnRegisterPage(): boolean {
    return (this.pageUrlPath == '/register');
  }

  ngOnInit(): void {
    // // Set the material theme.
    // let theme = GlobalThemeHelper.getInstance().getTheme();
    // this.document.getElementById('material-theme').setAttribute('href', theme);


    // testing
    let val1 = this.appConfig.get("key1");
    if(isDL()) dl.log("val1 = " + val1);
    // testing


    this.title = this.viewDataService.appTitle;
    // this.copyright = `Copyright &copy; 2017 ${this.title}`;   // tbd.
    // this.copyright = `Copyright &copy; 2017-${DateTimeUtil.getYear()} @realharry`;   // tbd.

    // this.copyright = `Copyright &copy; ${DateTimeUtil.getYear()} @realharry`;   // tbd.
    // this.copyright = `Copyright &copy; ${DateTimeUtil.getYear()} <a href="https://gitlab.com/realharry">@realharry</a>`;   // tbd.
    // this.copyright = `Copyright &copy; ${DateTimeUtil.getYear()} <a href="https://gitlab.com/jobclient">@jobclient</a>`;   // tbd.
    // this.copyright = `<a mat-button href="https://gitlab.com/jobclient">Copyright &copy; ${DateTimeUtil.getYear()} @jobclient</a>`;   // tbd.
    this.copyrightText = `Copyright &copy; ${DateTimeUtil.getYear()} @jobclient`;   // tbd.
    this.copyrightLink = "https://gitlab.com/jobclient";

    // ???
    // this.store.select('router').subscribe(value => {
    //   // temporary
    //   if(value) {
    //     this.routerPath = value.state.toString();
    //   } else {
    //     this.routerPath = '';
    //   }
    // });

    // ???
    // // this.routerPath = this.router.url;
    // this.routerPath = this.location.path();
    // ???

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        let url = (event as NavigationEnd).url;
        if (url.length > 1) {
          if (url.startsWith('/')) {
            url = url.substring(1);
          }
        }
        if (url.length > 0) {
          url = url.charAt(0).toUpperCase() + url.substring(1);
        }
        this.routerPath = url;
      });

    // tbd
    this.loginService.isAuthenticated().subscribe((status: LoggedInStatus) => {
      AuthStateHelper.getInstance().userAuthenticated = status.loggedIn;
    });


    // this.activatedRoute.url.subscribe(segments => {
    //   for(let s of segments) {
    //     if(isDL()) dl.log(`>>> Segment = ${s}`);
    //   }
    // });

    // if(isDL()) dl.log(`>>> URL = ${this.router.url}`);
    this.router.events.subscribe(event => {
      // if(isDL()) dl.log(`>>> Event = ${event}; URL = ${this.router.url}`);

      if(event instanceof ActivationEnd) {
        this.pageUrlPath = this.router.url;
        if(isDL()) dl.log(`>>> pageUrlPath = ${this.pageUrlPath}`);
      }
    });
  }


  routeHandler(routePath: string) {
    // [1] Without ngrx/router-store
    // this.router.navigate([routePath]);

    // [2] With ngrx/router-store
    this.store.dispatch(new RouterActions.Go({
      path: [routePath]
      // path: [routePath, { routeParam: 1 }],
      // query: { page: 1 },
      // extras: { replaceUrl: false }
    }));
  }

  // tbd
  private goBack() {
    this.store.dispatch(new RouterActions.Back());
  }
  private goForward() {
    this.store.dispatch(new RouterActions.Forward());
  }
  // ....



  handleClickAddJob() {
    if(isDL()) dl.log("'Add' button clicked.");

    // tbd

    let dialogRef = this.dialog.open(PostingEditorDialogComponent, {
      width: '350px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(isDL()) dl.log('The dialog was closed');
      // this.animal = result;
    });

  }


  // tbd:
  // Add button handlers


  // handleContextHelp() {
  //   if(isDL()) dl.log("'Help' button clicked.");    
  // }


  get hasSavedTheme(): boolean {
    // return this.globalThemeHelper.hasDefaultTheme();
    return this.globalThemeHandler.hasSavedTheme;
  }

  handleSaveCurrentTheme() {
    if(isDL()) dl.log("'Save Current' button clicked.");
    // this.globalThemeHelper.saveDefaultTheme();
    this.globalThemeHandler.handleSaveCurrentTheme();
  }

  // Use random theme (from now on, not just once)
  handleUseRandomTheme() {
    if(isDL()) dl.log("'Use Random' button clicked.");
    // this._useRandomTheme();
    this.globalThemeHandler.handleUseRandomTheme();
  }
  // private _useRandomTheme() {
  //   if (this.browserWindowService.document) {
  //     this.globalThemeHelper.removeDefaultTheme();
  //     let theme = this.globalThemeHelper.getDifferentTheme();
  //     // this.document.getElementById('material-theme').setAttribute('href', theme);
  //     this.browserWindowService.document.getElementById('material-theme').setAttribute('href', theme);
  //   } else {
  //     if(isDL()) dl.log("browserWindowService.document is null.");
  //   }
  // }

  handleTryDifferentThemes() {
    if(isDL()) dl.log("'Try Different' button clicked.");
    // this._tryDifferentTheme();
    this.globalThemeHandler.handleTryDifferentThemes();
  }
  // private _tryDifferentTheme() {
  //   if (this.browserWindowService.document) {
  //     let theme = this.globalThemeHelper.getDifferentTheme();
  //     // this.document.getElementById('material-theme').setAttribute('href', theme);
  //     this.browserWindowService.document.getElementById('material-theme').setAttribute('href', theme);
  //   } else {
  //     if(isDL()) dl.log("browserWindowService.document is null.");
  //   }
  // }




  // temporary
  get isUserLoggedIn(): boolean {
    // tbd
    // this.loginService.isAuthenticated().subscribe((status: LoggedInStatus) => {
    //   this.userAuthenticated = status.loggedIn;
    // });
    return AuthStateHelper.getInstance().userAuthenticated;
  }


  handleUserLogin() {
    if(isDL()) dl.log("'User Login' button clicked.");

    // tbd:
    this.openUserLoginDialog();

  }

  handleUserLogout() {
    if(isDL()) dl.log("'User Logout' button clicked.");

    // tbd:
    this.loginService.logout();
    AuthStateHelper.getInstance().userAuthenticated = false;
  }


  openUserLoginDialog() {
    let dialogRef = this.dialog.open(UserLoginModalComponent, {
      width: '280px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(loggedIn => {
      if(isDL()) dl.log('The dialog was closed. loggedIn = ' + loggedIn);

      if (loggedIn === true || loggedIn === false) {
        // this.ngAuthUserState.updateLoginStatus(loggedIn);
        AuthStateHelper.getInstance().userAuthenticated = loggedIn;
      }
    });
  }



  handleNewRegistration() {
    if(isDL()) dl.log("'Register' button clicked.");

    // TBD:
    // this.openNewRegistrationDialog();
    this.routeHandler('/register');
    // ...

  }

  // openNewRegistrationDialog() {
  //   if(isDL()) dl.log('openNewRegistrationDialog button clicked.');

  //   let dialogRef = this.dialog.open(NewRegistrationModalComponent, {
  //     width: '320px',
  //     data: {}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if(isDL()) dl.log('The dialog was closed. result = ' + result);
  //   });
  // }

  // openResendCodeDialog() {
  //   if(isDL()) dl.log('openResendCodeDialog button clicked.');

  //   let dialogRef = this.dialog.open(ResendCodeModalComponent, {
  //     width: '320px',
  //     data: {}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if(isDL()) dl.log('The dialog was closed. result = ' + result);
  //   });
  // }

  // openConfirmRegistrationDialog() {
  //   if(isDL()) dl.log('openConfirmRegistrationDialog button clicked.');

  //   let dialogRef = this.dialog.open(ConfirmRegistrationModalComponent, {
  //     width: '280px',
  //     data: {}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if(isDL()) dl.log('The dialog was closed. result = ' + result);
  //   });
  // }



  // handleShareJob() {
  //   if(isDL()) dl.log("'Share' button clicked.");    
  // }

  handleUserProfile() {
    if(isDL()) dl.log("'Profile' button clicked.");

    // tbd
    // This should be a page not a dialog???

    let dialogRef = this.dialog.open(UserProfileDialogComponent, {
      width: '350px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(isDL()) dl.log('The dialog was closed');
      // this.animal = result;
    });

  }

  handleUserSettings() {
    if(isDL()) dl.log("'User Settings' button clicked.");

    // tbd
    // This should be a page not a dialog???

    let dialogRef = this.dialog.open(UserSettingsDialogComponent, {
      width: '350px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(isDL()) dl.log('The dialog was closed');
      // this.animal = result;
    });

  }

}
