import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { JobPosting } from '@jobclient/model';

import { JobclientViewDataService } from '../../services/jobclient-view-data.service';


@Component({
  selector: 'jc-mini-job-record',
  templateUrl: './mini-job-record.component.html',
  styleUrls: ['./mini-job-record.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MiniJobRecordComponent implements OnInit {

  @Input("user-id") userId: string;
  @Input("job-id") jobId: string;

  constructor(private viewDataService: JobclientViewDataService) {
  }

  ngOnInit() {
  }

}
