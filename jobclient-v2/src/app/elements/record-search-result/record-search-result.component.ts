import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { JobRecord } from '@jobclient/model';

import { JobclientViewDataService } from '../../services/jobclient-view-data.service';


@Component({
  selector: 'jc-record-search-result',
  templateUrl: './record-search-result.component.html',
  styleUrls: ['./record-search-result.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RecordSearchResultComponent implements OnInit {

  @Input("user-id") userId: string;
  @Input("job-id") jobId: string;

  constructor(private viewDataService: JobclientViewDataService) {
  }

  ngOnInit() {
  }

}
