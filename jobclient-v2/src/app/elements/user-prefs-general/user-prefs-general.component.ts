import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { UserPreference } from '@jobclient/model';

import { JobclientViewDataService } from '../../services/jobclient-view-data.service';


@Component({
  selector: 'jc-user-prefs-general',
  templateUrl: './user-prefs-general.component.html',
  styleUrls: ['./user-prefs-general.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserPrefsGeneralComponent implements OnInit {

  @Input("user-id") userId: string;

  constructor(private viewDataService: JobclientViewDataService) {
  }

  ngOnInit() {
  }

}
