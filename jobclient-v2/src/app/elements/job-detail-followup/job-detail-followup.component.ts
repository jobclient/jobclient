import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { JobFollowup } from '@jobclient/model';

import { JobclientViewDataService } from '../../services/jobclient-view-data.service';


@Component({
  selector: 'jc-job-detail-followup',
  templateUrl: './job-detail-followup.component.html',
  styleUrls: ['./job-detail-followup.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class JobDetailFollowupComponent implements OnInit {

  @Input("user-id") userId: string;
  @Input("job-id") jobId: string;

  constructor(private viewDataService: JobclientViewDataService) {
  }

  ngOnInit() {
  }

}
