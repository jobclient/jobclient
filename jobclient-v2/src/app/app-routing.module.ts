import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// import { StoreRouterConnectingModule } from '@ngrx/router-store';

import { environment } from '../environments/environment';

import { NotFoundComponent } from './errors/not-found/not-found.component';

import { AppComponent } from './main/app.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { WorkspacePageComponent } from './pages/workspace-page/workspace-page.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { BrowsePageComponent } from './pages/browse-page/browse-page.component';
import { DetailPageComponent } from './pages/detail-page/detail-page.component';
import { FavoritesPageComponent } from './pages/favorites-page/favorites-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';


// temporary
const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: '/app',
  //   pathMatch: 'full'
  // },
  {
    path: 'dashboard',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: '',
    component: DashboardPageComponent
  },
  {
    path: 'workspace',
    component: WorkspacePageComponent
  },
  {
    path: 'browse',
    component: BrowsePageComponent
  },
  {
    path: 'favorites',
    component: FavoritesPageComponent
  },
  {
    path: 'search',
    component: SearchPageComponent
  },
  {
    path: 'register',
    component: RegisterPageComponent
  },
  {
    path: 'detail/:id',
    component: DetailPageComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }

];


// // Testing...
// var rmodule: ModuleWithProviders;
// if(environment.useHash) {
//   rmodule = RouterModule.forRoot(routes, {useHash: true});
// } else {
//   rmodule = RouterModule.forRoot(routes);
// }

@NgModule({
  imports: [
    // temporary.
    // GitLab Pages does not support multiple paths.
    // Until we find more permanent hosting site,
    // Use hash strategy, for now.
    // (Note: We need to use path strategy to be able to enable SSR.)
    // // RouterModule.forRoot(routes)  // Default is using path strategy.
    // RouterModule.forRoot(routes, {useHash: true})
    // // ...
    // rmodule
    RouterModule.forRoot(routes, {useHash: environment.useHash})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
