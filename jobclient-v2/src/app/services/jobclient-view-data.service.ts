import { Injectable } from '@angular/core';

@Injectable()
export class JobclientViewDataService {

  // temporary
  private _appTitle = "Job Client";

  constructor() { }

  get appTitle(): string {
    return this._appTitle;
  }

}
