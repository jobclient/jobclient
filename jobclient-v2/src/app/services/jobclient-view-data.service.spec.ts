import { TestBed, inject } from '@angular/core/testing';

import { JobclientViewDataService } from './jobclient-view-data.service';

describe('JobclientViewDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JobclientViewDataService]
    });
  });

  it('should be created', inject([JobclientViewDataService], (service: JobclientViewDataService) => {
    expect(service).toBeTruthy();
  }));
});
