import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';


@Injectable()
export class SnackBarHelper {

  constructor(
    private snackBar: MatSnackBar,
  ) {
  }

  showQuickMessage(message: string, action: string = 'Message') {
    this.openSnackBar(message, action, 1250, 250)
  }

  showWarningMessage(message: string, action: string = 'Warning') {
    this.openSnackBar(message, action, 1750, 250)
  }

  showErrorMessage(message: string, action: string = 'Error') {
    this.openSnackBar(message, action, 2500, 250)
  }

  openSnackBar(message: string, action: string, duration: number = 1000, delay: number = 0) {
    setTimeout(() => {
      this.snackBar.open(message, action, {
        duration: duration,
      });
    }, delay);
  }

}
